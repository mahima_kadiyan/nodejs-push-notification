const express = require('express');
const webPush = require('web-push');
const bodyparser = require('body-parser');
const path = require('path');
require('dotenv').config();

const app = express();
app.use(express.static(path.join(__dirname, 'client')));
app.use(bodyparser.json());

// VAPID keys should be generated only once.
// const vapidKeys = webPush.generateVAPIDKeys();

const vapidKeys = {
  publicKey: process.env.PUBLIC_VAPID_KEY,
  privateKey: process.env.PRIVATE_VAPID_KEY
}

webPush.setVapidDetails('mailto:test@test.com', vapidKeys.publicKey, vapidKeys.privateKey);

//subscirbe

app.post('/subscribe', (req, res) => {
  // get push 
  const subscription = req.body;

  // send status
  res.status(201).json({
    message: 'User Subscribed for Notification'
  });

  // create payload
  const payload = JSON.stringify({ title: 'Test Notification' });

  webPush.sendNotification(subscription, payload).catch(err => console.error(err));

  // this will keep on sending the notification
  // to show that closing the client doen't stop the notification from being sent or recieved
  // let i = 0;
  // setInterval(() => {
  //   i=i+1;
  //   const payload = JSON.stringify({ title: 'Test Notification', num:i });
  //   webPush.sendNotification(subscription, payload);
  // }, 2000);

});

const port = process.env.PORT ? process.env.PORT : 3000;
app.listen(port, () => console.log(`Server Started on Port ${port}`));