
const publicKey = 'BDHZCzx4hPvqyjRLcD1Y9jxpauFIcBFlZFXDnTZco1uSogqEFJLMfTWkuBgaWp0sYOkDGjkvx_Dvyml7o0BZWS8';

// we will check if the service worker is enabled in the current browser
// Navigator interface represents the state and the identity of the user agent
if ('serviceWorker' in navigator) {
    console.log('Browser supports Notifications!!!!');
}
// we will check if the site had permission to send notification
if (window.Notification) {
    Notification.requestPermission(() => {
        if (Notification.permission === 'granted') {
            notificationIsAllowed();
        } else {
            console.log('Permission Denied!!!!');
        }
    });
}

function notificationIsAllowed() {
    console.log('Permission granted for Notifications!!!!');
}

document.getElementById("mybtn").addEventListener("click", function () {
    send().catch(err => console.error(err));
});

// function triggered on button click
async function send() {
    // Returns a ServiceWorkerContainer object, which provides access to registration, removal, upgrade, and communication with the ServiceWorker objects for the associated document
    const register = await navigator.serviceWorker.register('./worker.js', {
        scope: '/'
    });
    const subscription = await register.pushManager.subscribe({
        userVisibleOnly: true,
        applicationServerKey: publicKey
    });
    // sending push Subscription object to server
    await fetch('/subscribe', {
        method: 'POST',
        body: JSON.stringify(subscription),
        headers: {
            'content-type': 'application/json'
        }
    });

}